/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import entites.Achat;
import java.util.*;
/**
 *
 * @author madara
 */
public class AchatService {
    
    private static LinkedList<Achat> ListeAchat = new LinkedList<>();

    public static LinkedList<Achat> getListeAchat() {
        return ListeAchat;
    }

    public static void setListeAchat(LinkedList<Achat> ListeAchat) {
        AchatService.ListeAchat = ListeAchat;
    }

    public void ajouter(Achat e){
        ListeAchat.add(e);
    }
    
    public void moidifier(Achat e){
        for(Achat element : ListeAchat){
            if(element.getId() == e.getId()){
                ListeAchat.add(ListeAchat.indexOf(element), e);
                ListeAchat.remove(element);
                break;
            }
        }
    }
    
    public void supprimer(long id){
        for(Achat element : ListeAchat){
            if(element.getId() == id){
                ListeAchat.remove(element);
                break;
            }
        }
    }
    
    public LinkedList<Achat> lister(Achat e){
        return AchatService.getListeAchat();
    }
    
    public Achat trouver(long id){
        for(Achat element : ListeAchat){
            if(element.getId() == id){
                return element;
            }
        }
        return null;
    }
    
    public int compter(){
        return ListeAchat.size();
    }
}
