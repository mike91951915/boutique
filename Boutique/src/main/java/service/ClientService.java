/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import entites.Client;
import java.util.*;
/**
 *
 * @author madara
 */
public class ClientService {
    
    private static LinkedList<Client> ListeClient = new LinkedList<>();

    public static LinkedList<Client> getListeClient() {
        return ListeClient;
    }

    public static void setListeClient(LinkedList<Client> ListeClient) {
        ClientService.ListeClient = ListeClient;
    }

    public void ajouter(Client e){
        ListeClient.add(e);
    }
    
    public void moidifier(Client e){
        for(Client element : ListeClient){
            if(element.getId() == e.getId()){
                ListeClient.add(ListeClient.indexOf(element), e);
                ListeClient.remove(element);
                break;
            }
        }
    }
    
    public void supprimer(long id){
        for(Client element : ListeClient){
            if(element.getId() == id){
                ListeClient.remove(element);
                break;
            }
        }
    }
    
    public LinkedList<Client> lister(Client e){
        return ClientService.getListeClient();
    }
    
    public Client trouver(long id){
        for(Client element : ListeClient){
            if(element.getId() == id){
                return element;
            }
        }
        return null;
    }
    
    public int compter(){
        return ListeClient.size();
    }
}
