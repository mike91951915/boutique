/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import entites.Produit;
import java.util.*;
/**
 *
 * @author madara
 */
public class ProduitService {
    
    private static LinkedList<Produit> ListeProduit = new LinkedList<>();

    public static LinkedList<Produit> getListeProduit() {
        return ListeProduit;
    }

    public static void setListeProduit(LinkedList<Produit> ListeProduit) {
        ProduitService.ListeProduit = ListeProduit;
    }

    public void ajouter(Produit e){
        ListeProduit.add(e);
    }
    
    public void moidifier(Produit e){
        for(Produit element : ListeProduit){
            if(element.getId() == e.getId()){
                ListeProduit.add(ListeProduit.indexOf(element), e);
                ListeProduit.remove(element);
                break;
            }
        }
    }
    
    public void supprimer(long id){
        for(Produit element : ListeProduit){
            if(element.getId() == id){
                ListeProduit.remove(element);
                break;
            }
        }
    }
    
    public LinkedList<Produit> lister(Produit e){
        return ProduitService.getListeProduit();
    }
    
    public Produit trouver(long id){
        for(Produit element : ListeProduit){
            if(element.getId() == id){
                return element;
            }
        }
        return null;
    }
    
    public int compter(){
        return ListeProduit.size();
    }
}
