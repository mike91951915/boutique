/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import entites.Employe;
import java.util.*;
/**
 *
 * @author madara
 */
public class EmployeService {
    
    private static LinkedList<Employe> ListeEmploye = new LinkedList<>();

    public static LinkedList<Employe> getListeEmploye() {
        return ListeEmploye;
    }

    public static void setListeEmploye(LinkedList<Employe> ListeEmploye) {
        EmployeService.ListeEmploye = ListeEmploye;
    }

    public void ajouter(Employe e){
        ListeEmploye.add(e);
    }
    
    public void moidifier(Employe e){
        for(Employe element : ListeEmploye){
            if(element.getId() == e.getId()){
                ListeEmploye.add(ListeEmploye.indexOf(element), e);
                ListeEmploye.remove(element);
                break;
            }
        }
    }
    
    public void supprimer(long id){
        for(Employe element : ListeEmploye){
            if(element.getId() == id){
                ListeEmploye.remove(element);
                break;
            }
        }
    }
    
    public LinkedList<Employe> lister(Employe e){
        return EmployeService.getListeEmploye();
    }
    
    public Employe trouver(long id){
        for(Employe element : ListeEmploye){
            if(element.getId() == id){
                return element;
            }
        }
        return null;
    }
    
    public int compter(){
        return ListeEmploye.size();
    }
}
