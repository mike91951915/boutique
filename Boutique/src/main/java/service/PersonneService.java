/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import entites.Personne;
import java.util.*;
/**
 *
 * @author madara
 */
public class PersonneService {
    
    private static LinkedList<Personne> ListePersonne = new LinkedList<>();

    public static LinkedList<Personne> getListePersonne() {
        return ListePersonne;
    }

    public static void setListePersonne(LinkedList<Personne> ListePersonne) {
        PersonneService.ListePersonne = ListePersonne;
    }

    public void ajouter(Personne e){
        ListePersonne.add(e);
    }
    
    public void moidifier(Personne e){
        for(Personne element : ListePersonne){
            if(element.getId() == e.getId()){
                ListePersonne.add(ListePersonne.indexOf(element), e);
                ListePersonne.remove(element);
                break;
            }
        }
    }
    
    public void supprimer(long id){
        for(Personne element : ListePersonne){
            if(element.getId() == id){
                ListePersonne.remove(element);
                break;
            }
        }
    }
    
    public LinkedList<Personne> lister(Personne e){
        return PersonneService.getListePersonne();
    }
    
    public Personne trouver(long id){
        for(Personne element : ListePersonne){
            if(element.getId() == id){
                return element;
            }
        }
        return null;
    }
    
    public int compter(){
        return ListePersonne.size();
    }
}
