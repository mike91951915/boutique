/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import entites.Categorie;
import java.util.*;
/**
 *
 * @author madara
 */
public class CategorieService {
    
    private static LinkedList<Categorie> ListeCategorie = new LinkedList<>();

    public static LinkedList<Categorie> getListeCategorie() {
        return ListeCategorie;
    }

    public static void setListeCategorie(LinkedList<Categorie> ListeCategorie) {
        CategorieService.ListeCategorie = ListeCategorie;
    }

    public void ajouter(Categorie e){
        ListeCategorie.add(e);
    }
    
    public void moidifier(Categorie e){
        for(Categorie element : ListeCategorie){
            if(element.getId() == e.getId()){
                ListeCategorie.add(ListeCategorie.indexOf(element), e);
                ListeCategorie.remove(element);
                break;
            }
        }
    }
    
    public void supprimer(long id){
        for(Categorie element : ListeCategorie){
            if(element.getId() == id){
                ListeCategorie.remove(element);
                break;
            }
        }
    }
    
    public LinkedList<Categorie> lister(Categorie e){
        return CategorieService.getListeCategorie();
    }
    
    public Categorie trouver(long id){
        for(Categorie element : ListeCategorie){
            if(element.getId() == id){
                return element;
            }
        }
        return null;
    }
    
    public int compter(){
        return ListeCategorie.size();
    }
}
