/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entites;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author madara
 */
public class Achat {
    
    private Long id;
    private LocalDateTime dateAchat;
    private Double remise = 0.0; 

    public Achat() {
    }

    public Achat(Long id, LocalDateTime dateAchat, Double remise) {
        this.id = id;
        this.dateAchat = dateAchat;
        this.remise = remise;
    }

    public Double getRemise() {
        return remise;
    }

    public void setRemise(Double remise) {
        this.remise = remise;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDateTime dateAchat) {
        this.dateAchat = dateAchat;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Achat other = (Achat) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Achat{" + "id=" + id + ", dateAchat=" + dateAchat + ", remise=" + remise + '}';
    }
            
    public Double getRemiseTotale(){
        return remise;
    }
    
    public Double getPrixTotal(){
        return 0.0;
    }
}
