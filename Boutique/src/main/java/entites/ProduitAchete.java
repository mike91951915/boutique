/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entites;

import java.util.Objects;

/**
 *
 * @author madara
 */
public class ProduitAchete {
    
    private int quantite = 1;
    private Double remise = 0.0;

    public ProduitAchete() {
    }

    public ProduitAchete(int quantite, Double remise) {
        this.quantite = quantite;
        this.remise = remise;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public Double getRemise() {
        return remise;
    }

    public void setRemise(Double remise) {
        this.remise = remise;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.quantite;
        hash = 97 * hash + Objects.hashCode(this.remise);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProduitAchete other = (ProduitAchete) obj;
        if (this.quantite != other.quantite) {
            return false;
        }
        return Objects.equals(this.remise, other.remise);
    }

    @Override
    public String toString() {
        return "ProduitAchete{" + "quantite=" + quantite + ", remise=" + remise + '}';
    }
    
    public Double getPrixTotal(){
        return 0.0;
    }
    
}
