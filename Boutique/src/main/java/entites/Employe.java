/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entites;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author madara
 */
public class Employe extends Personne{
    
    private String cnss;

    public Employe() {
        super();
    }

    public Employe(String cnss, Long id, String nom, String prenoms, LocalDate dateNaissance) {
        super(id, nom, prenoms, dateNaissance);
        this.cnss = cnss;
    }
    
    public String getCnss() {
        return cnss;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }

    @Override
    public String toString() {
        return "Employe{" + "cnss=" + cnss + ", id=" + id + ", nom=" + nom + ", prenoms=" + prenoms + ", dateNaissance=" + dateNaissance + '}';
    } 

}
